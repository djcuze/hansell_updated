Menu.create(
    name: 'Eggs and Toast',
    short_description: 'Eggs done your way- poached or fried w/ Turkish toast. GF available',
    long_description: 'Eggs done your way - poached or fried with Turkish toast...build your own & add sides',
    menu_type: 'breakfast')
Menu.create(
    name: 'Katz Pajamas',
    short_description: 'Old skool savoury mince on toast with poached egg, yes it has peas & carrot. GF Available',
    long_description: 'Old skool savoury mince on toast with poached egg, yes it has peas & carrot',
    menu_type: 'breakfast')
Menu.create(
    name: "Bubble 'n' Squeak",
    short_description: 'Corn, peas, carrot & potato hash w/ cheese, pea & mint salsa, egg. VEGETARIAN',
    long_description: "Corn, peas, carrot & potato hash smothered w/ bubbling halloumi cheese, smashed pea & mint salsa, fried egg...better than Nan's...Even better with a side of triple smoked bacon... choose your sides when your press order...",
    menu_type: 'breakfast')
Menu.create(
    name: 'Scooby Doo',
    short_description: 'Cheese kransky wrapped in bacon with fried egg & hollandaise, Turkish toast. GF Available',
    :long_description => "Cheese kransky wrapped in bacon with fried egg & hollandaise, Turkish toast...oooohh don't forget the sauce DUDE!",
    menu_type: 'breakfast')
Menu.create(
    name: 'Devilish Eggs',
    short_description: "Two of the world's hottest chillis- this is not for the faint hearted. VEGETARIAN | GF Available",
    long_description: "Using two of the world's hottest chillis- this is not for the faint hearted, you may need yoghurt after!..Eggs, rich spicy tomato ragu & halloumi on toasted Turkish.",
    menu_type: 'breakfast')
Menu.create(
    name: 'Matador',
    short_description: 'Tomato, chorizo, Aussie jack cheese, fried egg all baked in a clay pot. GF Available',
    long_description: 'Tomato, chorizo, Aussie jack cheese, fried egg all baked in a clay pot and lathered with chimichurri w/ toasted Turkish. Great with a side of halloumi!',
    menu_type: 'breakfast')
Menu.create(
    name: 'The Bogan Burger n hash browns',
    short_description: 'Fried egg with cheese, bacon, BBQ sauce on buttered Turkish toast.',
    long_description: 'Fried egg with cheese, bacon, BBQ sauce on buttered Turkish toast, this will get your mullet going. Add hollandaise for that extra V8 grunt!',
    menu_type: 'breakfast'
)

Menu.create(
    name: "Get Smashed",
    short_description: "Smashed avo & feta on sourdough! GF Availalbe",
    long_description: "Smashed avo & marinated feta on sourdough... why not add a poached egg and some bacon?",
    menu_type: 'breakfast')
Menu.create(
    name: "Bacon Eggs Benedict",
    short_description: "Our famous eggs benny..taste it & you'll know why!.. GF Available",
    long_description: "Our famous eggs benny with bacon..taste it & you'll know why!",
    menu_type: 'breakfast')
Menu.create(
    name: "Persian Ragu",
    short_description: "Slow cooked, Za'atar spiced Perisan tomato ragu. VEGAN | GF available",
    long_description: "Slow cooked, Za'atar spiced Persian tomato ragu  ginger marinated sweet potato crisps... Even better with a poached egg... It is said that Za'atar (a mixture of thyme, sumac and seasame seeds) gives strength and clears the mind.",
    menu_type: 'breakfast')
Menu.create(
    name: "Garlic Bread",
    short_description: "House made cheesy garlic bread!",
    long_description: "House made buttery yum yum garlic bread!",
    menu_type: 'breakfast')
Menu.create(
    name: "Toastie Royale",
    short_description: "Cheese toastie writ LARGE!",
    long_description: "Thick cut sourdough bread with cheese, more cheese, hollandaise and bacon... served with hash browns",
    menu_type: 'breakfast')

Menu.create(name: "Chips", short_description: "Grab a bowl of our 'gaucho' seasoned crispy chips with smokey mayo.", long_description: "Grab a bowl of our 'gaucho' seasoned crispy chips with smokey mayo.", menu_type: 'brunch')
Menu.create(name: "Chilli Cheese Fries", short_description: "Savoury mince, golden steak fries & Aussie jack cheese with HOT HOT sauce", long_description: "House savoury mince lathered on top of crunchy golden steak fries & a fistful of golden cheddar cheese, topped off with a squirt of HOT hot sauce featuring 2 of the world's hottest chillis - trinidad scorpion & habanero..PUPOW!!", menu_type: 'brunch')
Menu.create(name: "Bulgogi Beef Roll", short_description: "Soy, ginger, garlic beef in a 6 inch roll - Korean stylee", long_description: "Soy, ginger, garlic marinated beef rump in true Korean stylee nestled in a 6 inch roll with peppers and onions served with our crispy rice noodle pickled salad. ", menu_type: 'brunch')
Menu.create(name: "Charcoal Chicken Caesar Salad", short_description: "Short Description", long_description: "Shredded charcoal chicken, cos lettuce, grilled sweet bacon, smokey pimento aioli & crispy croutons... even better if you add a poached egg on top!", menu_type: 'brunch')
Menu.create(name: "Chickity China the Chinese Chicken", short_description: "GLUTEN FREE", long_description: "Char grilled HONEY SOY chicken, pickled ginger & carrot, rice noodles dresed in CHILLI chow chee (chilli & shallot slow cooked in soy and seasame, menu_type: 'brunch')", menu_type: 'brunch')
Menu.create(name: "Persian sweet potato salad", short_description: "GLUTEN FREE", long_description: "Ginger poached sweet potato, cos and shredded cabbage, pickled carrot & ginger, mint, coriander & crispy gluten free noodles", menu_type: 'brunch')
Menu.create(name: "Korean BBQ Beef Salad", short_description: "GLUTEN FREE", long_description: "Korean bulgogi BBQ beef, cos and shredded cabbage, pickled carrot & ginger, mint, coriander & crispy gluten free noodles", menu_type: 'brunch')
Menu.create(name: "Get Smashed!", short_description: "Smashed avo & feta on sourdough! GF Availalbe", long_description: "Smashed avo & marinated feta on sourdough... why not add a poached egg and some bacon?", menu_type: 'brunch')
Menu.create(name: "Persian Ragu!", short_description: "Slow cooked, Za'atar spiced Perisan tomato ragu. VEGAN | GF available ", long_description: "Slow cooked, Za'atar spiced Persian tomato ragu  ginger marinated sweet potato crisps... Even better with a poached egg... It is said that Za'atar (a mixture of thyme, sumac and seasame seeds, menu_type: 'brunch') gives strength and clears the mind.", menu_type: 'brunch')
Menu.create(name: "Scooby Doo!", short_description: "Cheese kransky wrapped in bacon with fried egg & hollandaise, Turkish toast. GF Available", long_description: "Cheese kransky wrapped in bacon with fried egg & hollandaise, Turkish toast...oooohh don't forget the sauce DUDE! Add a side of bacon to make a MAN-fast!", menu_type: 'brunch')
Menu.create(name: "The Bogan Burger n chips", short_description: "Fried egg with cheese, bacon, BBQ sauce on buttered Turkish toast.", long_description: "Fried egg with cheese, bacon, BBQ sauce on buttered Turkish toast, this will get your mullet going. Add hollandaise for that extra V8 grunt!", menu_type: 'brunch')
Menu.create(name: "Garlic Bread!", short_description: "House made cheesy garlic bread!", long_description: "House made cheesy garlic bread!", menu_type: 'brunch')
Menu.create(name: "Bacon Eggs Benedict!", short_description: "Our famous eggs benny..taste it & you'll know why!.. GF Available", long_description: "Our famous eggs benny with bacon..taste it & you'll know why!", menu_type: 'brunch')
Menu.create(name: "BBQ Beef Eggs Benedict!", short_description: "Our famous eggs benny..taste it & you'll know why!.. GF Available", long_description: "Our famous eggs benny with Korean BBQ beef..taste it & you'll know why!", menu_type: 'brunch')
Menu.create(name: "Chorizo Eggs Benedict!", short_description: "Our famous eggs benny..taste it & you'll know why!.. GF Available", long_description: "Our famous eggs benny with chorizo..taste it & you'll know why!", menu_type: 'brunch')
Menu.create(name: "Shroom Eggs Benedict!", short_description: "Our famous eggs benny..taste it & you'll know why!.. GF Available", long_description: "Our famous eggs benny with sauteed mushrooms..taste it & you'll know why!", menu_type: 'brunch')
Menu.create(name: "Guacamole Eggs Benedict!", short_description: "Our famous eggs benny..taste it & you'll know why!.. GF Available", long_description: "Our famous eggs benny with guacamole..taste it & you'll know why!", menu_type: 'brunch')
Menu.create(name: "Toastie Royale!", short_description: "Cheese toastie writ LARGE!", long_description: "Thick cut sourdough bread with cheese, more cheese, hollandaise and bacon... served with hash browns", menu_type: 'brunch')

