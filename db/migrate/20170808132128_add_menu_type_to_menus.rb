class AddMenuTypeToMenus < ActiveRecord::Migration[5.1]
  def change
    add_column :menus, :menu_type, :string
  end
end
