class CreateMenus < ActiveRecord::Migration[5.1]
  def change
    create_table :menus do |t|
      t.string :name
      t.string :short_description
      t.string :long_description

      t.timestamps
    end
  end
end
