Rails.application.routes.draw do
  root 'main#events'

  resources :menus
  get '/menu', to: 'menus#index', as: 'menus_path'
  get '/menu/new', to: 'menus#new'
  get '/functions', to: 'main#functions'
  get '/function_enquiry' => 'main#function_enquiry'
  post '/function_enquiry' => 'main#complete_function_enquiry', as: :functions_enquiries
end
