class FunctionsEnquiryMailer < ApplicationMailer

  def functions_enquiry_email(form)
    @form = form
    mail(
      to: Hng::Application.secrets.contact_email,
      subject: 'Functions Enquiry Requested'
    )
  end

  def reservations_email(form)
    @form = form
    mail(
      to: Hng::Application.secrets.contact_email,
      subject: 'Reservation Enquiry Requested'
    )
  end

end