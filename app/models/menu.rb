class Menu < ApplicationRecord

  validates :name, :short_description, :long_description, :menu_type, presence: true
end
