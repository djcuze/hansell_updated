require_relative 'modeless_form'

class FunctionsEnquiryForm < ModelessForm

  property :date
  property :time
  property :name
  property :occasion
  property :phone
  property :email
  property :number
  property :food_pkg
  property :drinks_pkg
  property :exclusivity

  def date
    Date.parse(super)
  rescue ArgumentError, TypeError
    super
  end

  def time
    Time.parse(super)
  rescue ArgumentError, TypeError
    super
  end

  validates :date, presence: true
  validates :time, presence: true
  validates :name, presence: true
  validates :occasion, presence: true
  validates :phone, presence: true
  validates :email, presence: true
  validates :number, presence: true
  validates :food_pkg, presence: true
  validates :drinks_pkg, presence: true
  validates :exclusivity, presence: true
end
