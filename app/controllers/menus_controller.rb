class MenusController < ApplicationController

  def new
    @menu = Menu.new
  end

  def create
    @menu = Menu.new(menu_params)
    puts menu_params
    respond_to do |format|
      if @menu.save
        format.html { redirect_to menus_path, notice: 'Menu Item was successfully created.' }
        format.json { render :show, status: :created, location: @menu }
      else
        format.html { render :new }
        format.json { render json: @menu.errors, status: :unprocessable_entity }
      end
    end
  end

  def index
    @title = 'Menu'
    @menu = Menu.all
  end

  def show
    @menu = Menu.find(params[:id])
    respond_to do |format|
      format.html
      format.js
    end
  end

  def destroy
    @menu = Menu.find(params[:id])
    @menu.destroy
    respond_to do |format|
      format.html { redirect_to menus_path, notice: 'Recipe was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def set_menu
    @menu = Menu.new
  end

  def menu_params
    params.require(:menu).permit(:name, :short_description, :long_description)
  end


end