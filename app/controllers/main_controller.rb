class MainController < ApplicationController

  def index
  end

  def events
    @title = 'Events'
  end

  def functions
    @title = 'Functions'
  end

  def privacy_policy
    @title = 'Privacy Policy'
  end

  def function_enquiry
    @title = 'Function Enquiry'
    @functions_enquiry = FunctionsEnquiryForm.new
  end

  def complete_function_enquiry
    @functions_enquiry = FunctionsEnquiryForm.new

    if @functions_enquiry.validate(params[:functions_enquiry])
      FunctionsEnquiryMailer.functions_enquiry_email(@functions_enquiry).deliver_now
      redirect_to :function_enquiry, notice: 'Your function enquiry has been submitted successfully'
    else
      render :function_enquiry
    end
  end
end
