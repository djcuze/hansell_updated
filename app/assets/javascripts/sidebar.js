$(document).ready(function () {
    var Body = function (element) {
        this.sidebar = document.getElementById('sidebar');
        this.handleEvent = function (event) {
            switch (event.type) {
                case 'click':
                    if (isMenu(event.target) || isSidebar(event.target)) {
                        this.showSidebar();
                    } else {
                        this.hideSidebar();
                    }
                    break;
            }
        };

        this.showSidebar = function () {
            sidebar.style.display = 'flex';
            document.getElementById('menu-toggle').style.right = '210px';
            document.getElementById('blur').style.display = 'block';

        };

        this.hideSidebar = function () {
            sidebar.style.display = 'none';
            document.getElementById('menu-toggle').style.right = '10px';
            document.getElementById('blur').style.display = 'none';
        };

        this.registerListener = function () {
            element.addEventListener('click', this, false);
        };

        function isMenu(element) {
            return element.id === 'menu-toggle' || element.parentNode.id === 'menu-toggle';
        }

        /* Simple add or remove OR operators if there are any elements inside your sidebar that close the sidebar on click */
        function isSidebar(element) {
            return element.id === 'sidebar' || element.parentNode.id === 'sidebar' || element.className === 'navigation__sub-menu--toggle' || element.className === 'navigation__link';
        }
    };
    var b = new Body(document.body);
    b.registerListener();
});
