$(document).ready(function () {
    $(".menu__tab").click(function () {
        var className = $(this).data('trigger');

        $('.menu__tab').removeClass('menu__tab--active');
        $('.menu > div').hide();
        $('.functions > div').hide();
        $("." + className).show();
        $(this).addClass('menu__tab--active')
    });
});